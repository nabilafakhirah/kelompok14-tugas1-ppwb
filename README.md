[![pipeline status](https://gitlab.com/nabilafakhirah/kelompok14-tugas1-ppwb/badges/master/pipeline.svg)](https://gitlab.com/nabilafakhirah/kelompok14-tugas1-ppwb/commits/master)

[![coverage report](https://gitlab.com/nabilafakhirah/kelompok14-tugas1-ppwb/badges/master/coverage.svg)](https://gitlab.com/nabilafakhirah/kelompok14-tugas1-ppwb/commits/master)


TUGAS 1 PPW.

Kelompok 14:
1. Arvin Raditya Niardi
2. Nabila Fakhirah Yusroni
3. Rai Indhira Saraswati
4. Selina Maurizka


Pada tugas 1 kali ini, hal-hal yang dipelajari adalah

1. Mengimplementasikan Disiplin Test Driven Development
2. Mengimplementasikan Unit Test
3. Mengimplementasikan konsep HTML dan Styling dengan CSS
4. Mengimplementasikan Responsive Web Design
5. Menggunakan Models untuk menyimpan data

Pada tugas 1, tahap awal yang kami lakukan adalah berdiskusi terkait pemberian nama aplikasi dan pembagian tugas. 
Berikut pembagian tugas:
1. Fitur'update status' dikerjakan oleh Rai Indhira
2. Fitur'halaman profile' dikerjakan oleh Nabila fakhirah yusroni
3. Fitur 'add-friend' oleh Arvin Raditya Niardi
4. Fitur 'statistik, dan dashboard' oleh Selina Maurizka


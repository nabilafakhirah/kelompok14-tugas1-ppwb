"""kilograms URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic.base import RedirectView
import addfriend.urls as addfriend
from addfriend.views import index as index_addfriend
import profile_page.urls as profile_page
import update_status.urls as index_update
import update_status.urls as update_status
import dashboard.urls as dashboard

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^addfriend/', include(addfriend,namespace='addfriend')),
    url(r'^profile-page/', include(profile_page, namespace='profile-page')),
	url(r'^update-status/', include(update_status, namespace='update-status')),
    url(r'^$', RedirectView.as_view(url='profile-page/', permanent="true"), name='index'),
    url(r'^dashboard/', include('dashboard.urls',namespace='dashboard')),
]

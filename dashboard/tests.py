from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from profile_page.models import ProfileContents, Expertise
from update_status.views import Todo

# Create your tests here.
class Tugas1UnitTest(TestCase):
	def setUp(self):
		self.profile = ProfileContents.objects.create(name = 'Saiton M. Si', birthday = "1996-2-28", gender = 'Male', description = 'I live for photography.', email = 'saitonisms@gmail.com', photo = 'http://78.media.tumblr.com/8fc11f08dc4e7a1ecf9de7d3a102a8c6/tumblr_oxioilcsKd1wdb06fo5_250.png')
		self.todo = Todo.objects.create(description='hehe')
		#new_activity = ProfileContents.objects.create(title='mengerjakan lab ppw', description='mengerjakan lab_5 ppw')

	def test_update_status_url_is_exist(self):
		response = Client().get('/dashboard/')
		self.assertEqual(response.status_code, 200)

	def test_update_status_using_index_func(self):
		found = resolve('/dashboard/')
		self.assertEqual(found.func, index)

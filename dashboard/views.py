from django.shortcuts import render
from addfriend.models import Message
from update_status.models import Todo
from profile_page.models import ProfileContents
from profile_page.views import index

# Create your views here.
response = {}
def index(request):
	if ProfileContents.objects.all().count!=0:
		p = ProfileContents.objects.first()
		response['name'] = p.name
		response['photo'] = p.photo
		response['friend'] = 0
		response['feed'] = 0
		if Todo.objects.all().count()!=0:
			todo = Todo.objects.last().description
			status_date = Todo.objects.last().created_date
			status_feeds = Todo.objects.all().count()
			response['latest_post'] = todo
			response['latest_post_date'] = status_date
			response['feed'] = status_feeds
			friends = Message.objects.all().count()
			response['friends'] = friends
	return render(request, "dashboard/dashboard.html", response)
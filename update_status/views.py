from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Todo_Form
from .models import Todo
from profile_page.models import ProfileContents

# Create your views here.
response = {}
def index(request):    
    response['author'] = "Rai Indhira" #TODO Implement yourname
    profile = ProfileContents.objects.last()
    response['name'] = profile.name
    response['photo'] = profile.photo
    todo = Todo.objects.all()
    todo = Todo.objects.order_by('-id')
    response['todo'] = todo
    html = 'update_status/update_status.html'
    response['todo_form'] = Todo_Form
    return render(request, html, response)
    

def add_todo(request):
    form = Todo_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['description'] = request.POST['description']
        todo = Todo(description=response['description'])
        todo.save()
        return HttpResponseRedirect('/update-status/')
    else:
        return HttpResponseRedirect('/update-status/')


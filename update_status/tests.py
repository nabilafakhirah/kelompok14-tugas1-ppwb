# Create your tests here.
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, add_todo
from .models import Todo
from .forms import Todo_Form
from profile_page.models import ProfileContents,Expertise

# Create your tests here.
class Lab5UnitTest(TestCase):
    def setUp(self):
        self.profile = ProfileContents.objects.create(name = 'Saiton M. Si', birthday = "1996-2-28", gender = 'Male', description = 'I live for photography.', email = 'saitonisms@gmail.com', photo = 'http://78.media.tumblr.com/8fc11f08dc4e7a1ecf9de7d3a102a8c6/tumblr_oxioilcsKd1wdb06fo5_250.png')

    def test_update_status_url_is_exist(self):
        response = Client().get('/update-status/')
        self.assertEqual(response.status_code, 200)


    def test_update_status_using_index_func(self):
        found = resolve('/update-status/')
        self.assertEqual(found.func, index)

    def test_model_can_create_new_todo(self):
        # Creating a new activity
        new_activity = Todo.objects.create(description='mengerjakan lab_5 ppw')

        # Retrieving all available activity
        counting_all_available_todo = Todo.objects.all().count()
        self.assertEqual(counting_all_available_todo, 1)


    def test_form_validation_for_blank_items(self):
        form = Todo_Form(data={'description': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
        form.errors['description'],
        ["This field is required."]
        )

    def test_lab5_post_error_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/update-status/add_todo', {'description': ''})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/update-status/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)
    
    def test_status_post_success_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/update-status/add_todo', {'description': test})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/update-status/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)






from django.shortcuts import render,redirect
from .forms import Message_Form
from .models import Message
from django.http import HttpResponseRedirect


landing_page_content = 'I am who I am. Not who you think I am. Not who you want me to be. I am me.'
response = {'author': "Arvin Raditya"} #TODO Implement yourname
about_me = ["Never", "Give Up", "On", "My", "Own", "Dream"]

def index(request):
    response['content'] = landing_page_content
    html = 'addfriend/addfriend.html'
    #TODO Implement, isilah dengan 6 kata yang mendeskripsikan anda
    response['about_me'] = about_me
    # return render(request, html, response)
    response['message_form'] = Message_Form
    message = Message.objects.all()
    response['message'] = message

    return render(request, html, response)



def message_post(request):
    form = Message_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['name'] = request.POST['name'] 
        # response['email'] = request.POST['email'] if request.POST['email'] != "" else "Anonymous"
        response['message'] = request.POST['message']
        message = Message(name=response['name'], 
                          message=response['message'])
        message.save()
        html ='addfriend/addfriend.html'
        
        # return redirect('/addfriend/')
    #     return render(request, html, response)
    # else:        
        return HttpResponseRedirect('/addfriend/')


# def message_table(request):
	
#     message = Message.objects.all()
#     response['message'] = message
#     html = 'addfriend/table.html'
#     return redirect('/addfriend/')
    # return render(request, html , response)


# Create your views here.

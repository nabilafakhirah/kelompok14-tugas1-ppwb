from django.db import models

# Create your models here.
class ProfileContents(models.Model):
    name = models.CharField(max_length=27)
    birthday = models.DateField()
    gender = models.CharField(max_length=6)
    expertise = models.ManyToManyField('Expertise')
    description = models.TextField()
    email = models.EmailField()
    photo = models.URLField()


    def __str__(self):
        return self.name

class Expertise(models.Model):
    expertise = models.CharField(max_length = 250)

    def __str__(self):
        return str(self.expertise)
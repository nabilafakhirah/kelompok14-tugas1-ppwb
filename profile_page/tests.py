from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from .models import ProfileContents, Expertise

# Create your tests here.
class ProfileUnitTest(TestCase):
    def test_profile_url_is_exist(self):
        response = Client().get('/profile-page/')
        self.assertEqual(response.status_code, 200)
    
    def test_root_url_now_is_using_index_page_from_status(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 301)
        self.assertRedirects(response, '/profile-page/', 301, 200)

    def test_profile_page_using_index_func(self):
        found = resolve('/profile-page/')
        self.assertEqual(found.func, index)

    def test_model_can_create_new_profile(self):
        new_profile = ProfileContents.objects.create(name = 'Americano Del Gado', birthday = "1990-1-1", gender = 'Unisex',
                                            description = 'Pahit sepahit kopi, pedas sepedas gado-gado.', email = 'kopicabe@pedas.com')
        counting_all_profiles = ProfileContents.objects.all().count()
        self.assertEqual(counting_all_profiles, 1)

    def test_tostring_function_in_profile(self):
        dummy_name = 'Americano Del Gado'
        profile = ProfileContents.objects.create(name = dummy_name, birthday = "1990-1-1", gender = 'Unisex',
                                            description = 'Pahit sepahit kopi, pedas sepedas gado-gado.', email = 'kopicabe@pedas.com')
        self.assertEqual(dummy_name, profile.__str__())
        

    def test_model_can_create_new_expertise(self):
        expertise = Expertise.objects.create(expertise = 'Nyabe')

        counting_all_expertise = Expertise.objects.all().count()
        self.assertEqual(counting_all_expertise, 1)                 

    def test_tostring_function_in_expertise(self):
        dummy_expertise = "Spicing"
        expertise = Expertise.objects.create(expertise = dummy_expertise)         

        self.assertEqual(dummy_expertise, expertise.__str__())          

    def test_profile_page_using_profile_page_html(self):
        response = Client().get('/profile-page/')
        self.assertTemplateUsed(response, 'profile_page.html') 
from django.shortcuts import render
from .models import ProfileContents, Expertise

# Create your views here.
response = {}

response['author'] = 'Nabila Fakhirah'

def index(request):
    profile = ProfileContents.objects.create( name = 'Saiton M. Si', birthday = "1996-2-28", gender = 'Male', description = 'I live for photography.', email = 'saitonisms@gmail.com',  photo = 'http://78.media.tumblr.com/8fc11f08dc4e7a1ecf9de7d3a102a8c6/tumblr_oxioilcsKd1wdb06fo5_250.png')
    profile.save()

    e1 = Expertise.objects.create(expertise = 'Photography')
    e1.save()
    e2 = Expertise.objects.create(expertise = 'Dodgeball')
    e2.save()
    e3 = Expertise.objects.create(expertise = 'Karaoke master')
    e3.save()

    profile.expertise.add(e1)
    profile.expertise.add(e2)
    profile.expertise.add(e3)

    response['photo'] = profile.photo
    response['name'] = profile.name
    response['birthday'] = profile.birthday
    response['gender'] = profile.gender
    response['expertise'] = profile.expertise.all()
    response['description'] = profile.description
    response['email'] = profile.email

    html = 'profile_page.html'

    return render(request, html, response)

